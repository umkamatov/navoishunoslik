import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
	// {
	//   path: '/',
	//   name: 'home',
	//   component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
	// },
	{
		path: "/",
		name: "home",
		component: () => import(/* webpackChunkName: "home" */ "../views/view-home.vue"),
	},
	{
		path: "/gallery",
		name: "gallery",
		component: () => import(/* webpackChunkName: "login" */ "../views/view-gallery.vue"),
	},
	{
		path: "/enc",
		name: "enc",
		component: () => import(/* webpackChunkName: "login" */ "../views/view-encyclopedia.vue"),
	},
	{
		path: "/faq",
		name: "faq",
		component: () => import(/* webpackChunkName: "login" */ "../views/view-faq.vue"),
	},
	{
		path: "/contacts",
		name: "contacts",
		component: () => import(/* webpackChunkName: "login" */ "../views/view-contacts.vue"),
	},
];

const router = new VueRouter({
	scrollBehavior(to, from, savedPosition) {
		if (savedPosition) {
			return savedPosition;
		} else {
			return { x: 0, y: 0 };
		}
	},
	mode: "history",
	base: process.env.BASE_URL,
	routes,
});

export default router;
