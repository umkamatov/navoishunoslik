// window.addEventListener("load", function () {
// 	const urlParams = new URLSearchParams(window.location.search);
// 	if (urlParams.has("lang")) {
// 		const langValue = urlParams.get("lang");
// 		const selector_items = document.querySelector(`.nv-selector__item[data-lang="${langValue}"]`);
// 		// selector_items.click();
// 		setLanguage(selector_items);
// 	}
// });

setTimeout(() => {
	// // menu burger
	// const br_opener = document.querySelector(".nv-header__burger");
	// const br_closer = document.querySelector(".nv-rubrics__toggler");
	// const br_rubrics = document.querySelector(".nv-rubrics");
	// br_opener.addEventListener("click", function () {
	// 	br_rubrics.classList.toggle("opened");
	// });
	// br_closer.addEventListener("click", function () {
	// 	br_rubrics.classList.remove("opened");
	// });
	// //gallery popup
	// document.addEventListener("click", function (event) {
	// 	const selector = document.querySelector(".nv-selector");
	// 	const isClickInside = selector.contains(event.target);
	// 	if (!isClickInside) {
	// 		selector.classList.remove("opened");
	// 	}
	// });
	// You might also need to prevent the default behavior of the click event inside the selector
	// document.querySelector(".nv-selector").addEventListener("click", function (event) {
	// 	event.stopPropagation();
	// });
	//language selector
	// const selector_select = document.querySelector(".nv-selector");
	// const selector_toggler = document.querySelector(".nv-selector__toggler");
	// const selector_items = document.querySelectorAll(".nv-selector__item");
	// const acitve_label = document.querySelector(".nv-selector__toggler-label");
	// const active_icon = document.querySelector(".nv-selector__toggler-icon");
	// const available_items = document.querySelectorAll(".fl-item");
	// // Filter the list based on data-lang value
	// function filterList(lang) {
	// 	available_items.forEach((item) => {
	// 		const dataLang = item.getAttribute("data-lang");
	// 		item.style.display = lang === "ALL" || dataLang === lang ? "flex" : "none";
	// 	});
	// }
	// function setLanguage(item) {
	// 	console.log("ITEMS", item);
	// 	const langValue = item.dataset.lang;
	// 	const langLabel = item.dataset.label;
	// 	const langFlag = item.querySelector("img").getAttribute("src");
	// 	acitve_label.innerHTML = langLabel;
	// 	active_icon.setAttribute("src", langFlag);
	// 	selector_select.classList.toggle("opened");
	// 	const currentUrl = new URL(window.location.href);
	// 	currentUrl.searchParams.set("lang", langValue);
	// 	window.history.replaceState({}, document.title, currentUrl.href);
	// 	filterList(langValue);
	// }
	// selector_items.forEach((item) => {
	// 	item.addEventListener("click", () => {
	// 		setLanguage(item);
	// 	});
	// });
	// selector_toggler.addEventListener("click", function () {
	// 	selector_select.classList.toggle("opened");
	// });
	// const faq_cards = document.querySelectorAll(".nv-faq__card");
	// faq_cards.forEach((item) => {
	// 	item.addEventListener("click", () => {
	// 		item.classList.toggle("opened");
	// 	});
	// });

	const TOKEN = "6762585672:AAHgAVELrep7-YtwntToVNvV-tO6wBaiW1Y";
	const chatID = "-4054758391";
	// const TOKEN = "1195077988:AAGYUpfLUgHDNIBjWXNH8dYCCc2XSXizq7k";
	// const chatID = "-529378526";
	const api = "https://api.telegram.org/bot" + TOKEN + "/sendMessage?chat_id=" + chatID + "&parse_mode=html&text=";

	function passContact(e) {
		e.preventDefault();

		let name = document.getElementById("contact_name").value.replace(/ /g, "%20");
		let phone = document.getElementById("contact_phone").value.replace(/ /g, "%20");
		let title = document.getElementById("contact_title").value.replace(/ /g, "%20");
		let text = document.getElementById("contact_message").value.replace(/ /g, "%20");

		var message2 = `Bog'lanish formasidan yangi xabar: %0A%0A<b>Xabar jo'natuvchi ismi: </b> ${name} %0A<b>Xabar jo'natuvchi telefon raqami: </b> ${phone} %0A<b>Xabarning mazmuni: </b> ${title} %0A<b>Xabarning matni: </b> ${text} `;

		fetch(api + message2).then((result) => {
			document.getElementById("contact_name").value = null;
			document.getElementById("contact_phone").value = null;
			document.getElementById("contact_title").value = null;
			document.getElementById("contact_message").value = null;

			if (result.ok) {
				document.getElementById("custommodal__success").classList.add("show");
				setTimeout(() => {
					document.getElementById("custommodal__success").classList.remove("show");
				}, 5000);
			} else {
				document.getElementById("custommodal__error").classList.add("show");
				setTimeout(() => {
					document.getElementById("custommodal__error").classList.remove("show");
				}, 5000);
			}
		});
	}

	// passContact();

	function passOffer(e) {
		let name = document.getElementById("offer_name").value.replace(/ /g, "%20");
		let phone = document.getElementById("offer_phone").value.replace(/ /g, "%20");
		let title = document.getElementById("offer_title").value.replace(/ /g, "%20");
		let text = document.getElementById("offer_message").value.replace(/ /g, "%20");

		var message2 = `Takliflar formasidan yangi taklif: %0A%0A<b>Xabar jo'natuvchi ismi: </b> ${name} %0A<b>Xabar jo'natuvchi telefon raqami: </b> ${phone} %0A<b>Xabarning mazmuni: </b> ${title} %0A<b>Xabarning matni: </b> ${text} `;

		fetch(api + message2).then((result) => {
			document.getElementById("offer_name").value = null;
			document.getElementById("offer_phone").value = null;
			document.getElementById("offer_title").value = null;
			document.getElementById("offer_message").value = null;

			if (result.ok) {
				document.getElementById("custommodal__success").classList.add("show");
				setTimeout(() => {
					document.getElementById("custommodal__success").classList.remove("show");
				}, 5000);
			} else {
				document.getElementById("custommodal__error").classList.add("show");
				setTimeout(() => {
					document.getElementById("custommodal__error").classList.remove("show");
				}, 5000);
			}
		});
	}
}, 1000);
